
var date_time = setInterval(dateTime, 1000)

// funtion local time
function dateTime() {
    var date = new Date();
    document.getElementById("current-date").innerHTML =date.toDateString() + " " + date.toLocaleTimeString();;
}

var btnStart = true;
var btnStop = false;
var btnClear = false;
var startTime;
var stopTime;
var price;

function start_time() {
    if (btnStart) {
        startTime = new Date();
        document.getElementById("start-time").innerHTML = '<div class="flex text-center items-center gap-1"><i class="fa-solid fa-circle-play"></i> Start at</div>' + startTime.toLocaleTimeString();;

        document.getElementById("btn-start").innerHTML = 
        '<div class="flex text-center items-center gap-1"><i class="fa-solid fa-stop"></i> Stop</div>'

        document.getElementById('btn-start').style.backgroundColor = 'red'
        btnStart = false;
        btnStop = true;
    }
    else if(btnStop){
        var stopTime = new Date();
        // document.getElementById("stop-time").innerHTML = "Stop at " + stopTime.toLocaleTimeString();;
        document.getElementById("stop-time").innerHTML = '<div class="flex text-center items-center gap-1"><i class="fa-solid fa-stop"></i> Stop at</div>' + startTime.toLocaleTimeString();;

        document.getElementById("btn-start").innerHTML = 
        '<div class="flex text-center items-center gap-1"><i class="fa-solid fa-trash-can"></i> Clear</div>'
        // document.getElementById("btn-start").innerHTML = 'Clear'

        document.getElementById('btn-start').style.backgroundColor = 'orange'
        // document.getElementById('btn-start').innerHTML = 

        // if() else
         var minute_play = (stopTime.getHours()*60 + stopTime.getMinutes())-(startTime.getHours()*60 +  startTime.getMinutes());
        // var minute_play = 190;

        
        if(minute_play <= 15){
            document.getElementById('minutes').innerHTML = minute_play + "Minute(s)";
            document.getElementById("price").innerHTML = "500 Riels"
        }
        else if(minute_play<=30){
            document.getElementById('minutes').innerHTML = minute_play + "Minute(s)";
            document.getElementById("price").innerHTML = "1000 Riels"
        }
        else if(minute_play<=60){
            document.getElementById('minutes').innerHTML = minute_play + "Minute(s)";
            document.getElementById("price").innerHTML = "1500 Riels"
        }else{
            var min = minute_play%60; 
            var over_min= Math.trunc(minute_play/60);
            if(min>0 && min <=15){
                price = over_min*1500 + 500;
            }else if(min>0 && min<=30){
                price = over_min*1500 + 1000;
            }else if(min>0 && min < 60){
                price = over_min*1500 + 1500;
            }else{
                price = over_min*1500; 
            }
            document.getElementById("minutes").innerHTML = minute_play + " Minute(s)"

         
            document.getElementById('price').innerHTML = price + "Riel(s)";
        }

        btnStop = false;
        btnClear = true;
    }
    else{
        document.getElementById("start-time").innerHTML = "Start at 0:00";
        document.getElementById("stop-time").innerHTML = "Stop at 0:00";
        document.getElementById("minutes").innerHTML = "0 minute(s)";
        document.getElementById("price").innerHTML = "0 Riel(s)";

        document.getElementById("btn-start").innerHTML = 
        '<div class="flex text-center items-center gap-1 "><i class="fa-solid fa-circle-play"></i> Start</div>'
        document.getElementById("btn-start").style.backgroundColor="green"
        btnClear = false;
        btnStart = true;
    }

}