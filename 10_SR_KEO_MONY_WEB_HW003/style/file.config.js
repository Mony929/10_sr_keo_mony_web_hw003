
tailwind.config = {
    theme: {
        extend: {
            colors: {
                "text_color": "#ECF0F1"
            },
            backgroundImage:{
                "bg_image1" : "url('/images/bg-img.webp')"
            },
            backgroundColor:{
                "bg_color1": "#F08080"
            }
        }
    }
}




